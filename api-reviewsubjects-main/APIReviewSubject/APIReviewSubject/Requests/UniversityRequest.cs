﻿namespace APIReviewSubject.Requests
{
    public class UniversityRequest
    {
        public string name { get; set; }
        public string address { get; set; }
    }
}
