﻿using System;

namespace APIReviewSubject.Requests
{
    public class LikeRequest
    {
        public int postId { get; set; }
        public int userId { get; set; }
    }
}
