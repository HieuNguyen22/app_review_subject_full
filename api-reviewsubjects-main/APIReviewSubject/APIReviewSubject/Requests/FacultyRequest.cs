﻿namespace APIReviewSubject.Requests
{
    public class FacultyRequest
    {
        public string name { get; set; }
        public int universityId { get; set; }
    }
}
